import React, { Component } from 'react';
import { TouchableOpacity, Text, View } from 'react-native';


export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      titleGroup: [
        { id: 1, name: 'Title - 1', areChildrenVisible: false },
        { id: 2, name: 'Title - 2', areChildrenVisible: false },
        { id: 3, name: 'Title - 3', areChildrenVisible: false },
      ],
      subtitleGroup: [
        { id: 1, name: 'Subtitle - 1', titleId: 1, areChildrenVisible: false },
        { id: 2, name: 'Subtitle - 2', titleId: 2, areChildrenVisible: false },
        { id: 3, name: 'Subtitle - 3', titleId: 3, areChildrenVisible: false },
      ],
      listGroup: [
        { id: 1, name: 'List - 1', subtitleId: 1 },
        { id: 2, name: 'List - 2', subtitleId: 2 },
        { id: 3, name: 'List - 3', subtitleId: 3 },
      ],
    };
  }

  _onPressTitle = (titleId) => {
    this.setState({
      ...this.state,
      subtitleGroup: this.state.subtitleGroup.map(m => ({ ...m, areChildrenVisible: false })),
      titleGroup: this.state.titleGroup.map((m) => {
        if (m.id === titleId) return { ...m, areChildrenVisible: true };
        return { ...m, areChildrenVisible: false };
      }),
    });
  }

  _onPressSubtitle = (subtitleId) => {
    this.setState({
      ...this.state,
      subtitleGroup: this.state.subtitleGroup.map((m) => {
        if (m.id === subtitleId) return { ...m, areChildrenVisible: true };
        return { ...m, areChildrenVisible: false };
      }),
    });
  }

  _areChildrenVisibleForTitleId = (titleId) => {
    let areChildrenVisible = false;
    this.state.titleGroup.forEach((m) => {
      if (m.id === titleId) areChildrenVisible = m.areChildrenVisible;
    });
    return areChildrenVisible;
  }

  _areChildrenVisibleForSubtitleId = (subtitleId) => {
    let areChildrenVisible = false;
    this.state.subtitleGroup.map((m) => {
      if (m.id === subtitleId) areChildrenVisible = m.areChildrenVisible;
    });
    return areChildrenVisible;
  }

  _renderTitleGroup = () => this.state.titleGroup.map((m, index) => {
    console.log('rendering', m);
    return (
      <TouchableOpacity
        key={`${index} ${m.name}`}
        onPress={() => this._onPressTitle(m.id)}
      >
        <Text>{m.name}</Text>
      </TouchableOpacity >);
  })

  _renderSubtitleGroup = () => this.state.subtitleGroup.map((m, index) => {
    if (this._areChildrenVisibleForTitleId(m.titleId) === false) return null;
    console.log('rendering', m);
    return (
      <TouchableOpacity
        key={`${index} ${m.name}`}
        onPress={() => this._onPressSubtitle(m.id)}
      >
        <Text>{m.name}</Text>
      </TouchableOpacity >
    );
  })

  _renderListGroup = () => this.state.listGroup.map((m, index) => {
    if (this._areChildrenVisibleForSubtitleId(m.subtitleId) === false) return null;
    return (<TouchableOpacity
      key={`${index} ${m.name}`}
    >
      <Text>{m.name}</Text>
    </TouchableOpacity >
    );
  })

  render() {
    console.log('state', this.state);
    return (
      <View style={{ flex: 1, marginTop: 60, marginLeft: 20, marginRight: 20, flexDirection: 'row', justifyContent: 'center' }}>
        <View style={{ flex: 1 }}>
          {this._renderTitleGroup()}
        </View>
        <View style={{ flex: 1 }}>
          {this._renderSubtitleGroup()}
        </View>
        <View style={{ flex: 1 }}>
          {this._renderListGroup()}
        </View>
      </View>
    );
  }
}
